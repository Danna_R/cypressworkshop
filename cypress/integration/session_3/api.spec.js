/// <reference types="Cypress" />

    context("Cypress.Request", () => {

        var name = 'NewProd'
        
        it("Add and Remove a Product via API", () => {
            cy.addProduct(name, '$200.3')
            .then(function (response) {
                expect(response.status).to.eq(201)
                return response.body.id                           
            }) 
            .then(id => {
                cy.getProduct(id)
                .then(function (response) {
                    expect(response.status).to.eq(200)
                    expect(response.body.regular_price).to.eq('200.3')
                })
                const data = {
                    product_id: id,
                    review: "Adding a review!",
                    reviewer: "Danna",
                    reviewer_email: "drueda@gap.com"
                };
                cy.addReview(data)
                .then(function (response) {
                    expect(response.status).to.eq(201)                            
                
                })
                cy.visit('/shop/?orderby=date&paged=1')
                cy.get('[class="woocommerce-loop-product__title"]').should('be.visible').and('contain', name)
                //cy.get('[class="woocommerce-loop-product__title"]').first().invoke('text')
                //.then(title => {
                //    expect(title).to.eql('NewProd')
                //})
                cy.deleteProduct(id)
                .then(function (response) {
                    expect(response.status).to.eq(200)
                })
            })
          
        }) 
     
    })