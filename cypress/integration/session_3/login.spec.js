/// <reference types="Cypress" />
import { loginPageElements } from '../../pageLocators/login.locators'
import { myAccountPageElements } from '../../pageLocators/my_account.locators'

describe('Login Tests', () => {

    it ('Login via UI', () => {
        cy.visit('/my-account')
        cy.get(loginPageElements.userAccountTitle).should('be.visible')
        cy.get(loginPageElements.usernameTxt).type('session3 ')
        cy.get(loginPageElements.passwordTxt).type('3oC(52bCeYqVD(Oq^%T615rS')
        cy.get(loginPageElements.loginBtn).click()
        cy.get(myAccountPageElements.myAccountTittle).should('be.visible')
    })

    
})