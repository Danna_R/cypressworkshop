/// <reference types="Cypress" />
import { shopPageElements } from '../../pageLocators/shop.locators'
import { carPageElements } from '../../pageLocators/car.locators'

describe('Product Tests', () => {

    it ('Add a product to the cart', () => {
        cy.visit('/shop')
        cy.get(shopPageElements.shopTittle).should('be.visible').and('contain', 'Shop')
        cy.get(shopPageElements.shirtProduct).eq(0).click()
        cy.visit('/cart')
        cy.get(carPageElements.carTittle).should('be.visible').and('contain', 'Cart')
        cy.get(carPageElements.carProduct).should('be.visible')
    })

    it ('Delete a product from the cart', () => {
        cy.get(carPageElements.removeProductIcn).click()
        cy.get(carPageElements.removeProductText).should('be.visible').and('have.text', 'Your cart is currently empty.')
    })

    


})