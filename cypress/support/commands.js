// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

        Cypress.Commands.add('addProduct', (name, regular_price) => {
            cy.request(
            { method: 'POST',
                url: Cypress.config('apiUrl') + `wc/v3/products/?name=${name}&regular_price=${regular_price}`,
                form: true,
                headers: { 'Authorization': 'Basic ZHJ1ZWRhOmRydWVkYQ==' }
                //formData: { name: 'ws-Prod123', regular_price: '100.3' }
            })
        })

        Cypress.Commands.add('getProduct', (id) => {
            cy.request(
            { method: 'GET',
                url: Cypress.config('apiUrl') + `wc/v3/products/${id}`,
                form: true,
                headers: { 'Authorization': 'Basic ZHJ1ZWRhOmRydWVkYQ==' }})
        })

        Cypress.Commands.add('addReview', (data) => {
            cy.request(
              { method: 'POST',
                url: Cypress.config('apiUrl') + 'wc/v3/products/reviews',
                form: true,
                headers: { 'Authorization': 'Basic ZHJ1ZWRhOmRydWVkYQ==' },
                body: data
            })
                
        })

        Cypress.Commands.add('deleteProduct', (id) => {
            cy.request(
            { method: 'DELETE',
                url: Cypress.config('apiUrl') + `wc/v3/products/${id}`,
                form: true,
                headers: { 'Authorization': 'Basic ZHJ1ZWRhOmRydWVkYQ==' }
            }) 
        })
